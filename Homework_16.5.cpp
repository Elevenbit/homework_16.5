// Homework_16.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <ctime>


int main()
{
    const int N = 2;
    int array[N][N] = { {0, 1}, {2, 3} };

    std::time_t t = std::time(0);   // get time now
    std::tm now;
    localtime_s(&now, &t);
    int current_day = now.tm_mday;

    int sum = 0;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j];
            std::cout << '\n';
            

            if (current_day % N == i)
            {
                sum = sum + array[i][j];
            }
            
        }
        
    }
    std::cout << sum;
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
